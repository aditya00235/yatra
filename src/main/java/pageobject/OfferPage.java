package pageobject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import reusablecomponents.ReusableMethods;
import uistore.OfferPageUI;

public class OfferPage 
{
	public static boolean validateDomesticFlightTab(WebDriver driver,Logger log)
	{
		if(ReusableMethods.getElements(OfferPageUI.domesticFlightTab, driver))
		{
			log.info("Domestic Flight Identified");
			return true;
		}
		return false;
	}

}
