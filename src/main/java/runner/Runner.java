package runner;

import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageobject.HomePage;
import reusablecomponents.Base;
import reusablecomponents.GenerateProp;

public class Runner 
{
	WebDriver driver;
	Properties prop;
	Logger log=Logger.getLogger(Runner.class);
	@BeforeClass
	public void setDriver(ITestContext context) throws IOException
	{
	  driver = Base.initializeDriver();
	  context.setAttribute("WebDriver", driver);
	}
	
	  
	@BeforeTest
	public void initialize() throws IOException
	{
		driver=Base.initializeDriver();
	}
	
	
	@Test(priority=0)
	
	public void bookTickets() throws InterruptedException, Exception
	{
		prop=GenerateProp.generatePropertyObject();
		driver.get(prop.getProperty("url"));
		boolean flag=HomePage.bookFlights(driver, log);
		try
		{
		Assert.assertEquals(flag,true);
		}
		catch(Exception e)
		{
			log.error("Not booked");
		}
	}
	
	@Test(priority=1)
	public void clickOfferSection() throws Exception
	{
		prop=GenerateProp.generatePropertyObject();
		driver.get(prop.getProperty("url"));
		driver.manage().window().maximize();
		//driver.switchTo().alert().dismiss();
		boolean flag=HomePage.clickOffer(driver, log);
		try {
		Assert.assertEquals(flag,true);
		}
		catch(Exception e)
		{
			log.error("Offer section not found");
		}
	}
	
	@Test(priority=2)
	public void validateTitleTravelGuru() throws Exception
	{
		prop=GenerateProp.generatePropertyObject();
		driver.get(prop.getProperty("url"));
		driver.manage().window().maximize();
		//driver.switchTo().alert().dismiss();
		boolean flag=HomePage.travelGuruIconNavigation(driver, log);
				if(flag)
				{
					Set<String> s=driver.getWindowHandles();
					Iterator<String> it=s.iterator();
					String parent=it.next();
					String child=it.next();
					driver.switchTo().window(child);
					String titleTravelGuru=driver.getTitle();
					if(titleTravelGuru.equals("Travel Guru"))
					{
						log.info("Title matched");
						Assert.assertEquals(flag,true);
					}
					else {
						log.warn("Title not matched");
					}
					driver.switchTo().window(parent);
				}
	}
	
	@AfterTest
	public void tearDown()
	{
		driver.close();
		driver=null;
	}

}
